-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 05, 2017 at 02:37 
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wwwtek_prosjekt2`
--

-- --------------------------------------------------------

--
-- Table structure for table `emne`
--

CREATE TABLE `emne` (
  `emnekode` varchar(12) COLLATE utf8_bin NOT NULL,
  `navn` varchar(60) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `emne`
--

INSERT INTO `emne` (`emnekode`, `navn`) VALUES
('IMT3333', 'Data 3'),
('IMT6666', 'Hard Data');

-- --------------------------------------------------------

--
-- Table structure for table `pameldt`
--

CREATE TABLE `pameldt` (
  `userid` int(11) NOT NULL,
  `emnekode` varchar(12) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `pameldt`
--

INSERT INTO `pameldt` (`userid`, `emnekode`) VALUES
(2, 'IMT3333'),
(3, 'IMT3333'),
(4, 'IMT6666');

-- --------------------------------------------------------

--
-- Table structure for table `playlist`
--

CREATE TABLE `playlist` (
  `playlistid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `playlistvideo`
--

CREATE TABLE `playlistvideo` (
  `playlistid` int(11) NOT NULL,
  `videoid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `subtitle`
--

CREATE TABLE `subtitle` (
  `videoid` int(11) NOT NULL,
  `lang` varchar(3) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `subtitle`
--

INSERT INTO `subtitle` (`videoid`, `lang`) VALUES
(1, 'EN');

-- --------------------------------------------------------

--
-- Table structure for table `subtitlelang`
--

CREATE TABLE `subtitlelang` (
  `lang` varchar(3) COLLATE utf8_bin NOT NULL,
  `name` varchar(30) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `subtitlelang`
--

INSERT INTO `subtitlelang` (`lang`, `name`) VALUES
('EN', 'English'),
('DE', 'German'),
('JP', 'Japanese'),
('NO', 'Norwegian');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `userid` int(11) NOT NULL,
  `userclass` enum('admin','foreleser','student') COLLATE utf8_bin NOT NULL DEFAULT 'student',
  `email` varchar(60) COLLATE utf8_bin NOT NULL,
  `username` varchar(60) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userid`, `userclass`, `email`, `username`, `password`) VALUES
(1, 'admin', 'admin@admin.com', 'admin', '$2y$10$CBLe0bpoPKTVQ5XEPxUAm.O9qeiwYsktQJBMhLP0vJ0D7qUbXt1TO'),
(2, 'student', 'student1231312@student.com', 'Student1', '$2y$10$0EKr1SwAjJmmJ3S5nBbaqO1MrFm/z.6Cwwn.9uWlw4fuK1ic/Jiy2'),
(3, 'student', 'student453453443@student.com', 'Student2', '$2y$10$VFHexwMZn7GsHJMia3i65O7moUyef92sCCihrNKrFwIXp92N8scy2'),
(4, 'student', 'student4348573245723@student.com', 'Student3', '$2y$10$QmL..CJcB/4zuKcPn1Wzde/c7HUMQKJVMw7gEYp66jn5mJFQTyq4O');

-- --------------------------------------------------------

--
-- Table structure for table `usersession`
--

CREATE TABLE `usersession` (
  `userid` int(11) NOT NULL,
  `seriesid` varchar(255) COLLATE utf8_bin NOT NULL,
  `tokenid` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `videoid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `title` varchar(60) COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`videoid`, `userid`, `title`, `description`, `date`) VALUES
(1, 1, 'Vape Nation', 'Ethan fra h3h3 productions viser oss hvordan man river en feit en.', '2017-05-05 14:33:07');

-- --------------------------------------------------------

--
-- Table structure for table `videostats`
--

CREATE TABLE `videostats` (
  `userid` int(11) NOT NULL,
  `videoid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `emne`
--
ALTER TABLE `emne`
  ADD PRIMARY KEY (`emnekode`);

--
-- Indexes for table `pameldt`
--
ALTER TABLE `pameldt`
  ADD PRIMARY KEY (`userid`,`emnekode`),
  ADD KEY `emnekode` (`emnekode`);

--
-- Indexes for table `playlist`
--
ALTER TABLE `playlist`
  ADD PRIMARY KEY (`playlistid`),
  ADD KEY `userid` (`userid`);

--
-- Indexes for table `playlistvideo`
--
ALTER TABLE `playlistvideo`
  ADD PRIMARY KEY (`playlistid`,`videoid`),
  ADD KEY `videoid` (`videoid`);

--
-- Indexes for table `subtitle`
--
ALTER TABLE `subtitle`
  ADD PRIMARY KEY (`videoid`,`lang`),
  ADD KEY `lang` (`lang`);

--
-- Indexes for table `subtitlelang`
--
ALTER TABLE `subtitlelang`
  ADD PRIMARY KEY (`lang`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userid`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `usersession`
--
ALTER TABLE `usersession`
  ADD PRIMARY KEY (`userid`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`videoid`);

--
-- Indexes for table `videostats`
--
ALTER TABLE `videostats`
  ADD PRIMARY KEY (`userid`,`videoid`),
  ADD KEY `videoid` (`videoid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `playlist`
--
ALTER TABLE `playlist`
  MODIFY `playlistid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `videoid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `pameldt`
--
ALTER TABLE `pameldt`
  ADD CONSTRAINT `pameldt_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user` (`userid`),
  ADD CONSTRAINT `pameldt_ibfk_2` FOREIGN KEY (`emnekode`) REFERENCES `emne` (`emnekode`);

--
-- Constraints for table `playlist`
--
ALTER TABLE `playlist`
  ADD CONSTRAINT `playlist_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user` (`userid`);

--
-- Constraints for table `playlistvideo`
--
ALTER TABLE `playlistvideo`
  ADD CONSTRAINT `playlistvideo_ibfk_1` FOREIGN KEY (`playlistid`) REFERENCES `playlist` (`playlistid`),
  ADD CONSTRAINT `playlistvideo_ibfk_2` FOREIGN KEY (`videoid`) REFERENCES `video` (`videoid`);

--
-- Constraints for table `subtitle`
--
ALTER TABLE `subtitle`
  ADD CONSTRAINT `subtitle_ibfk_1` FOREIGN KEY (`lang`) REFERENCES `subtitlelang` (`lang`);

--
-- Constraints for table `usersession`
--
ALTER TABLE `usersession`
  ADD CONSTRAINT `usersession_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user` (`userid`);

--
-- Constraints for table `videostats`
--
ALTER TABLE `videostats`
  ADD CONSTRAINT `videostats_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user` (`userid`),
  ADD CONSTRAINT `videostats_ibfk_2` FOREIGN KEY (`videoid`) REFERENCES `video` (`videoid`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
