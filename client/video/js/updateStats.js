jwplayer().on('complete', function(){
	console.log('Video has ended!');
	
	vidForm = new FormData();
	vidForm.append('videoID', video['video']);
	$.ajax({
		url: 'server/php/videoStats.php?function=updateStats',
		type: 'POST',
        data: vidForm,
        cache: false,
        dataType: 'json',
        processData: false,
        contentType: false,
	});
	delete vidForm;
});