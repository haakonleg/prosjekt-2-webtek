// Add to playlist button
    $.ajax({
        url: 'server/php/playlist.php?function=getPlaylists',
        dataType: 'json',
        success: function (data) {
            $playlists = $('.dropdown-menu[id="playlist"]');
            if (data.length < 1) {
                $playlists.html('<h4>No playlists found.</h4>');
            } else {
                $.each(data, function(i, pl) {
                    $playlists.append('<li><a href="" id="' + pl['playlistid'] + '">' + pl['name'] + '</a></li>');
                });
            }
        }
    });

// Playlist click events
$('.dropdown-menu').on('click', 'a', function (event) {
    event.preventDefault();
    // Add to playlist
    $.ajax({
      url: 'server/php/playlist.php?function=addVideo',
      type: 'POST',
      data: {plID: this.id, videoID: video['video']},
      success: function(data) {
        console.log('added');
      }
    });
});

// If show playlist
if (plID > 0) {
  $.ajax({
    url: 'server/php/playlist.php?function=getPlaylistVideos',
    type: 'POST',
    data: {plID: plID},
    dataType: 'json',
    success: function (data) {
      // Add videos to playlist
      $playlist = $('.list-inline');
      $('#playlist').show();
      $.each(data, function(i, videoid) {
        $.ajax({
          url: 'server/php/getvideo.php',
          async: false,
          dataType: 'json',
          type: 'POST',
          data: {videoid: videoid[0]},
          success: function (response) {
            video = response;
            $playlist.append(
              '<li class="list-inline-item col clearfix">' +
              '<a href="" id="'+ i +'" class="list-inline-item">' +
                '<img src="' + video[0]['thumbFile'] + '" class="img-thumbnail pull-left" width="200" height="200" style="margin-right: 20px;">' +
                '<p>' + video[0]['description'] + '</p>' +
              '</a>' +
              '</li>'
            );
            // Add this video to jwplayer playlist
            addVideo(video[0]['videoFile'], video[0]['thumbFile'], video[0]['title'], video[0]['description']);
          }
        });
      });

      // Add event listeners to playlist buttons
      $playlist.on('click', 'a', function(event) {
        event.preventDefault();
      });

      // Play first video in playlist
      jwplayer().playlistItem(0);
    },
    error: function (data) {
      console.log(data);
    }
  });
}