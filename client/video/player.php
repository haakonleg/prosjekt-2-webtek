<div class="row">
  <div id="player-container">
    <div id="player"></div>
	<div id="img"><img id="pic" width="55%" src=""/></div>
    <div id="sidebar">
      <div id="searchbox">
        <span id="match">0 of 0</span>
        <input type="search" id="search" />
      </div>
      <div id="transcript"></div>
    </div>
  </div>
</div>

<div class="row dropup" style="padding: 10px;">
    <button class="btn btn-primary dropdown-toggle btn-sm" type="button" data-toggle="dropdown">Add to playlist <span class="caret"></span></button>
    <ul class="dropdown-menu" id="playlist">
    </ul>
</div>

<!-- Playlist -->
<div class="panel panel-default" id="playlist" style="display: none;">
    <div class="panel-heading">Playlist</div>
    <div class="panel-body">
        <ul class="list-inline" style="overflow: auto;">
        </ul>
    </div>
</div>

<!--Lar bruker lage ny vtt fil.-->

<div class="panel panel-default">
  <div class="panel-heading">Subtitle Editor</div>
  <div class="panel-body" id="tekst_editor"></div>
</div>

<!--Kode hvor tekst editor ligger:-->
<script>$('#tekst_editor').load('client/video/js/online_vtt_editor.html');</script>
<script>
// Retrieves video information from an ajax request (via getvideo.js) with POST
var video = <?php echo isset($_POST) ? json_encode($_POST) : null ?>;

// If plID is set to something else than 0, show playlist of this ID
var plID = <?php echo isset($_GET['plID']) ? $_GET['plID'] : 0 ?>;

videoElement = {
  playlist: [{
    file: video['videoFile'],
    image: video['thumbFile'],
    title: video['title'],
    description: video['description'],
	 tracks: [{ 
            file: video['captionFile'], 
            kind: "captions",
            label: "nan"
        }],
  },],
  width: 640,
  height: 360,
};

chapterFile = video['chapterFile'];
captionFile = video['captionFile'];
</script>
<script src="client/video/jwplayer-7/jwplayer.js"></script>
<script src="client/video/js/main.js"></script>
<script src="client/video/js/updateStats.js"></script>
<script src="client/video/js/speed.js"></script>
<script src="client/video/js/playlist.js"></script>
<script src="client/video/js/minify.js"></script>
<button type="button" class="btn" id="testbut">test</button>