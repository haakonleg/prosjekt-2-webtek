/* 
--------------------------------------------------------------------
    ADMINPANEL.JS
    This script controls everything to do with the admin panel page
---------------------------------------------------------------------
*/

$usertable = $('.panel-heading:contains("Delete user")').next('.panel-body').find('table');

// Shows registrated users    
function updateUserTable() {
    $usertable.find('tbody').empty();
    $.ajax({
        url: 'server/php/admin.php',
        type: 'POST',
        data: {function: 'getUserList'},
        dataType: 'json',
        success: function(response)
        {
            if(response.length < 1)
            {
            $usertable.find('tbody').html('<h4>No users found</h4>');
            } else {
                $.each(response, function(i, user) {
                    $usertable.find('tbody').append(
                        '<tr>' +
                        '<td>' + user['email'] + '</td>' +
                        '<td>' + user['username'] + '</td>' +
                        '<td>' + user['userclass'] + '</td>' +
                        '<td><button type="submit" class="btn btn-danger btn-xs btn-block" value="' + user['userid'] + '"><span class="glyphicon glyphicon-remove"></span></button></td>' +
                        '</tr>'
                    );
                });
            }
        }
    });
}

// Click event for delete user
$usertable.on('click', '.btn', function () {
    $button = $(this);
    $.ajax({
        url: 'server/php/admin.php',
        type: 'POST',
        data: {function: 'delUser', userid: $button.val()},
        success: function(response)
        {
            if (response == 'ok') {
                $('#adminalert').addClass('alert-success').text('User deleted').slideDown('fast');
                $button.closest('tr').empty();
            } else {
                $('#adminalert').addClass('alert-danger').text('Could not delete user').slideDown('fast');
            }
        }
    });
});

// Submit event for new user form
$userform = $('.panel-heading:contains("Create new user")').next('.panel-body').find('form');
$userform.submit(function (event) {
    event.preventDefault();
    
    var data = $userform.serializeArray();
    
    $.ajax({
        url: 'server/php/admin.php',
        type: 'POST',
        data: { function: 'newUser',
                email: data[0]['value'], 
                username: data[1]['value'], 
                password: data[2]['value'], 
                userclass: data[3]['value'] },
        success: function(response)
        {
            if (response == 'ok') {
                $('#adminalert').addClass('alert-success').text('User added successfully').slideDown('fast');
                updateUserTable();
            } else {
                $('#adminalert').addClass('alert-danger').text('Could not add user').slideDown('fast');
            }
            $userform[0].reset();
        }
    });
});

// Add users to the "delete user" list
updateUserTable();