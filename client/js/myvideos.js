// Get the users videolist
var videos = [];

$.ajax({
    url: 'server/php/myvideos.php',
    dataType: 'json',
    data: {function: 'getVideos'},
    success: function (response) {
        videos = JSON.parse(response);
        // Add videos to video list
        $list = $('.list-group');
        $.each(videos, function(i, id) {
            $.ajax({
                url: 'server/php/getvideo.php',
                dataType: 'json',
                type: 'POST',
                data: {videoid: id[0]},
                success: function (response) {
                    video = response;
                    $list.append(
                        '<li class="list-group-item col clearfix">' +
                            '<a href="" id="' + video[0]['videoid'] + '">' +
                                '<img src="' + video[0]['thumbFile'] + '" class="img-thumbnail pull-left" width="200" height="200" style="margin-right: 20px;">' +
                                '<h3>' + video[0]['title'] + '</h3>' +
                                '<p>' + video[0]['description'] + '</p>' +
                                '</a>' +
                            '<button type="button" class="btn btn-default btn-sm" id="' + video[0]['videoid'] + '"">Delete</button>' +
                        '</li></a>'
                    );
                }
            });
        });
    },
    error: function(response) {
        console.log(response);
    }
});

// Click event for videos
$('.list-group').on('click', 'a', function (event) {
    event.preventDefault();
    $('#content').hide().html(getVideo(this.id)).fadeIn(300);
});

// Delete video click event
$('.list-group').on('click', 'button', function () {
    $btn = $(this);
    $.ajax({
        url: 'server/php/myvideos.php?function=delVideo',
        type: 'POST',
        data: {videoid: this.id},
        success: function (response) {
            if(response == 'ok') {
                $('.alert').addClass('alert-success').text('Video deleted').slideDown('fast');
                $btn.closest('li').remove();
            } else {
                $('.alert').addClass('alert-danger').text(response).slideDown('fast');                
            }
        }
    })
});