/* 
--------------------------------------------------------------------
    NAVBAR.JS
    Takes care of everything that relates to the navbar, such as click events for navbar links
    and submit event for the navbar login form
---------------------------------------------------------------------
*/


/*
    JQUERY QUERIES
*/

// Get content div
$content = $('#content');
// Get all navbar links
$navlinks = $('.navbar-nav[id^="nav"]').find('a').add('#adminpanel');
// Get login form
$form = $('#nav-login');
// Get dropdown menus
$logindropdown = $('#loginmenu');
$userdropdown = $('#usermenu');
// Get navlinks
$navdefault = $('#navdefault');
$navlogin = $('#navlogin');
// Get search form
$search = $('form[role="search"]');


// Update navbar
updateNavbar();

/*
    GLOBAL VARIABLES
*/
var searchresults;

/*
    EVENT LISTENERS
*/

// Click events for navlinks
$navlinks.click(function (event) {
    event.preventDefault();
    
    switch (this.id) {
        case 'home':
            loadContent('client/html/home.html');
            document.title = 'uLearn';
            break;
        case 'upload':
            loadContent('client/html/upload.html', 'client/js/upload.js');
            document.title = 'uLearn - Upload';
            break;
        case 'playlists':
            loadContent('client/html/playlists.html', 'client/js/playlist.js');
            document.title = 'uLearn - Playlists';
            break;
        case 'videos':
            loadContent('client/html/videos.html', 'client/js/myvideos.js');
            document.title = 'uLearn - Videos';
            break;
        case 'adminpanel':
            loadContent('client/html/adminpanel.html', 'client/js/adminpanel.js');
            document.title = 'uLearn - Admin';
            break;
		case 'vidStat':
            loadContent('client/html/videoStats.html', 'client/js/videoStats.js');
            document.title = 'uLearn - Vid stats';
            break;
		case 'testPage':
            loadContent('client/html/test.html');
            document.title = 'uLearn - Test page';
            break;
    }

    // Remove active class from buttons
    $navlinks.parent().removeClass('active');

    // Add "active" class to button
    $(this).parent().addClass('active');
});

// Submit event for login form
$form.submit(function (event) {
    event.preventDefault();
    
    // Get form data
    var data = $form.serialize();

    // Send login request
    $.ajax({
        url: 'server/php/login.php',
        type: 'POST',
        dataType: 'html',
        data: data,
        success: function(response)
        {
            if (response) {
                // Do something if login is success
                user = JSON.parse(response);
                loggedIn = true;
                updateNavbar();
            } else {
                // Do something if login is fail
                $('.alert').slideDown('fast').text('Error: wrong username/password.');
            }
            $form[0].reset();
        }
    });
});

// Hide login error message on form focus
$form.find('input').focus(function () {
    $('.alert').slideUp('fast');
});

// Click event for logout button
$userdropdown.find('#logoutbtn').click(function() {
    $.ajax({
        url: 'server/php/logout.php',
        success: function(response)
        {
            if (response == 'ok') {
                loggedIn = false;
                $content.load('client/html/home.html');
                updateNavbar();
            } else {
                console.log('Error: unable to log out');
            }
        }
    });
});

// Search submit event
$search.submit(function (event) {
    event.preventDefault();

    var data = $search.serialize().toLowerCase();

    // Send search query
    $.ajax({
        url: 'server/php/search.php',
        type: 'POST',
        data: data,
        dataType: 'json',
        success: function(results) {
            if (results.length < 1) {
                $('#noResults').modal()
            } else {
                // Show search results
                searchresults = results;
                loadContent('client/html/search.html', 'client/js/search.js');
            }
        },
        error: function (response) {
            console.log(response);
        }
    });
});

// Change navbar when user is logged in or logged out
function updateNavbar() {
    if(loggedIn) {
        $logindropdown.hide();
        $userdropdown.show();
        $userdropdown.find('a').html(user['username'] + ' <span class="glyphicon glyphicon-user"></span>');
        $navdefault.hide();
        $navlogin.show();
        if (user['userclass'] == 'admin') {
            $navlinks.filter('#adminpanel').show();
        }
    } else {
        $userdropdown.hide();
        $logindropdown.show();
        $navlogin.hide();
        $navdefault.show();
        $navlinks.filter('#adminpanel').hide();
    }
}

// Loads content into the content div
function loadContent(url, script) {
    $content.hide().load(url, function() {
        console.log('Loaded ' + url);
        if (script) {
            $.getScript(script);
            console.log('Loaded ' + script);
        }
    }).fadeIn(300);
}