/*
videoStats.js is responsable with accsesing videoStats.php

*/

$.ajax({
	url: 'server/php/videoStats.php?function=getVid',
	// type: 'POST',
    cache: false,
    dataType: 'json',
    processData: false,
    contentType: false,
	success: function(data)
    {
		//http://stackoverflow.com/questions/19901843/display-json-data-in-html-table
		console.log(data);
		if(data){
            var len = data.length;
            var txt = "";
            if(len > 0){
                for(var i=0;i<len;i++){
                    if(data[i].title && data[i].videoid){
                        txt += "<option value='" +data[i].videoid+ "' >"+data[i].title+"</option>";
                    }
                }
                if(txt != ""){
                    $("#vidID").append(txt);
                }
            }
        }
	}
});


$.ajax({
	url: 'server/php/videoStats.php?function=getEK',
	// type: 'POST',
    cache: false,
    dataType: 'json',
    processData: false,
    contentType: false,
	success: function(data)
    {
		//http://stackoverflow.com/questions/19901843/display-json-data-in-html-table
		console.log(data);
		if(data){
            var len = data.length;
            var txt = "";
            if(len > 0){
                for(var i=0;i<len;i++){
                    if(data[i].emnekode){
                        txt += "<option>"+data[i].emnekode+"</option>";
                    }
                }
                if(txt != ""){
                    $("#emmneKode").append(txt);
                }
            }
        }
	}
});

$("#getStat").on('click', function(e)
{
	searchForm = new FormData();
	searchEk = $('#emmneKode').val();
	searchVid = $('#vidID').val();
	console.log(searchVid);
	searchForm.append('videoID', searchVid);
	searchForm.append('emmneKode', searchEk);
    console.log('BTN PRESS!!!');
	$.ajax({
		url: 'server/php/videoStats.php?function=getStats',
		type: 'POST',
        data: searchForm,
        cache: false,
        dataType: 'json',
        processData: false,
        contentType: false,
		success: function(data)
        {
            //console.log(response);
			//data = response;
			//http://stackoverflow.com/questions/19901843/display-json-data-in-html-table
			console.log(data);
			if(data){
                var len = data.length;
                var txt = "";
                if(len > 0){
                    for(var i=0;i<len;i++){
                        if(data[i].title && data[i].username){
                            txt += "<tr><td>"+data[i].title+"</td><td>"+data[i].username+"</td></tr>";
                        }
                    }
                    if(txt != ""){
						$("#table > tbody").empty();
                        $("#table").append(txt).removeClass("hidden");
						$("#table").addClass("table");
                    }
                }
            }
		}
	});
	delete searchForm;
});