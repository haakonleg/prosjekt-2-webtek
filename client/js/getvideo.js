/*
    GETVIDEO.JS
    This script sends an ajax reuqest to the server to get the video details
    then it feeds the variables to jwplayer and returns the DOM document of the video page
*/

function getVideo(id) {
    var videoDOM;
    var video;
    
    $.ajax({
        url: 'server/php/getvideo.php',
        type: 'POST',
        async: false,
        data: {videoid: id},
        success: function (response) {
            try {
                video = JSON.parse(response);
            } catch (e) {
                console.log(response);
            }
            $.ajax({
                url: 'client/video/player.php',
                type: 'POST',
                dataType: 'html',
                async: false,
                data: {
                    video: video[0]['videoid'],
                    description: video[0]['description'],
                    title: video[0]['title'],
                    userid: video[0]['userid'],
                    date: video[0]['date'],
                    videoFile: video[0]['videoFile'],
                    captionFile: video[0]['captionFile'],
                    chapterFile: video[0]['chapterFile'],
                    thumbFile: video[0]['thumbFile']
                },
                success: function (data) {
                    videoDOM = data;
                }
            });
        },
        error: function (response) {
            console.log(response);
        }
    });
    return videoDOM;
}

function getPlaylist(id) {
    var playlistDOM;
    
    $.ajax({
        url: 'client/video/player.php',
        type: 'GET',
        data: {plID: id},
        dataType: 'html',
        async: false,
        success: function (data) {
            playlistDOM = data;
        },
        error: function (data) {
            console.log(data);
        }
    });    
    return playlistDOM;
}