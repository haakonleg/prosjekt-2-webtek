$('h4[id="searchText"]').text('Found ' + searchresults.length + ' results:');
$list = $('.list-group');
$.each(searchresults, function(i, id) {
    $.ajax({
        url: 'server/php/getvideo.php',
        dataType: 'json',
        type: 'POST',
        data: {videoid: id[0]},
        success: function (response) {
            video = response;
            $list.append(
                '<a href="" id="' + video[0]['videoid'] + '" li class="list-group-item col clearfix">' +
                        '<img src="' + video[0]['thumbFile'] + '" class="img-thumbnail pull-left" width="200" height="200" style="margin-right: 20px;">' +
                        '<h3>' + video[0]['title'] + '</h3>' +
                        '<p>' + video[0]['description'] + '</p>' +
                '</li></a>'
            );
        },
        error: function (response) {
            console.log(response);
        }
    });
});

// Click event for videos
$('.list-group').on('click', 'a', function (event) {
    event.preventDefault();
    $('#content').hide().html(getVideo(this.id)).fadeIn(300);
});