// Create new playlist form
$('#plForm').submit(function(event) {
    event.preventDefault();
    var data = $(this).serialize();
    
    $.ajax({
        url: 'server/php/playlist.php?function=createPlaylist',
        type: 'POST',
        data: data,
        success: function(response) {
            
        }
    });

    $('#collapse1').collapse('hide');
    $(this)[0].reset();
});

updatePlaylists();

// Updates the users playlists overview
function updatePlaylists() {
    $.ajax({
        url: 'server/php/playlist.php?function=getPlaylists',
        dataType: 'json',
        success: function (data) {
            $playlists = $('.list-group');
            if (data.length < 1) {
                $playlists.html('<h4>No playlists found.</h4>');
            } else {
                $.each(data, function(i, pl) {
                    $playlists.append(
                        '<li class="list-group-item col clearfix">' +
                            '<a href="" id="' + pl['playlistid'] + '">' +
                                '<h3>' + pl['name'] + '</h3>' +
                                '<p>' + pl['description'] + '</p>' +
                                '</a>' +
                            '<button type="button" class="btn btn-default btn-sm" id="' + pl['playlistid'] + '"">Delete</button>' +
                        '</li></a>'
                    );
                });
            }
        }
    });
}

// Playlist click events
$('.list-group').on('click', 'a', function (event) {
    event.preventDefault();
    $('#content').hide().html(getPlaylist(this.id)).fadeIn(300);
});

$('.list-group').on('click', 'button', function () {
    $btn = $(this);
    $.ajax({
        url:'server/php/playlist.php?function=delPlaylist',
        type: 'POST',
        data: {plID: this.id},
        success: function(response) {
            console.log(response);
            if(response == 'ok') {
                $('.alert').addClass('alert-success').text('Playlist deleted').slideDown('fast');
                $btn.closest('li').remove();
            }
        }
    });
});