// Send AJAX request to get user info
var user = '';
var loggedIn = false;

$.ajax({
    url: 'server/php/login.php',
    async: false,
    dataType: 'json',
    success: function(response)
    {
        if (response) {
            user = response;
            loggedIn = true;
        }
    }
});