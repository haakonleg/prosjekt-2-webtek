/*
    UPLOAD.JS
    Handles uploading of videos
*/

// Video and thumbnail file
var vidFile;
var vidTitle;
var vidDescription;
var thumbFile;

// Subtitle files
var subFiles = [];
var subLang = [];

// Add subtitle languages to language select
$.ajax({
    url: 'server/php/upload.php?function=getLanguages',
    dataType: 'json',
    success: function(response)
    {
        $.each(response, function(i, language) {
            $('#subUpload').find('select').append(
                '<option value ="' + language['lang'] + '">' + language['name'] + '</option>'
            );
        });
    }
});

// Video upload event listener
$('#vidUpload').find('#videoFile').change(function() {
    $alert = $(this).closest('.panel-body').find('.alert');
    $alert.hide();

    file = this.files[0];
    title = $(this).closest('.panel-body').find('#title').val();
    description = $(this).closest('.panel-body').find('#description').val();

    // Check title and description
    if (title.length < 3 || description.length < 3) {
        $alert.addClass('alert-danger').text('Enter title and description of 3 characters or more').slideDown('fast');
        return false;
    }

    // Check file size
    if ((file.size) > 120000000) {
        $alert.addClass('alert-danger').text('File is too large').slideDown('fast');
        return false;
    }

    // Check if file extension is legal
    switch((file.name).split('.').pop().toLowerCase()) {
        case 'mp4':
            break;
        case 'avi':
            break;
        case 'wmv':
            break;
        case 'mpg':
            break;
        case '3gp':
            break;
        default:
            $alert.text('Invalid file type').slideDown('fast');
            return false;
    }

    // Push video
    vidFile = file;
    vidTitle = title;
    vidDescription = description;
});

$('#vidUpload').find('#thumbFile').change(function() {
    $alert = $(this).closest('.panel-body').find('.alert');
    $alert.hide();

    file = this.files[0];

    // Check file size
    if ((file.size) > 5000000) {
        $alert.addClass('alert-danger').text('File is too large').slideDown('fast');
        return false;
    }

        // Check if file extension is legal
    switch((file.name).split('.').pop().toLowerCase()) {
        case 'jpg':
            break;
        case 'jpeg':
            break;
        case 'png':
            break;
        case 'gif':
            break;
        default:
            $alert.addClass('alert-danger').text('Invalid file type').slideDown('fast');
            return false;
    }

    // Push thumbnail file to array
    thumbFile = file;
});

// Caption upload event listener
$('#subUpload').find('input:eq(0)').change(function() {
    $alert = $(this).closest('.panel-body').find('.alert');
    $alert.hide();
    
    file = this.files[0];

    // Check file size
    if ((file.size) > 1000000) {
        $alert.text('File is too large').slideDown('fast');
        return false;
    }

    // Check if file extension is legal
    if ((file.name).split('.').pop() != 'vtt') {
        $alert.text('Invalid file type').slideDown('fast');
        return false;
    }

    // Push file and lang to array to store
    subFiles[0] = this.files[0];
});

// Chapter upload event listener
$('#subUpload').find('input:eq(1)').change(function() {
    $alert = $(this).closest('.panel-body').find('.alert');
    $alert.hide();
    
    file = this.files[0];

    // Check file size
    if ((file.size) > 1000000) {
        $alert.text('File is too large').slideDown('fast');
        return false;
    }

    // Check if file extension is legal
    if ((file.name).split('.').pop() != 'vtt') {
        $alert.text('Invalid file type').slideDown('fast');
        return false;
    }

    // Push file and lang to array to store
    subFiles[1] = this.files[0];
});


// Language select
$('#subUpload').find('select').change(function () {
    subLang[0] = $(this).val();
});

// Upload button event listener
$('#uploadBtn').click(function () {
    $(this).text('Uploading...');
    $alert = $(this).siblings('.alert');
    $alert.hide();
    //advarsel om videofil ikke er lastet opp
    if(!vidFile) {
        $alert.addClass('alert-danger').text('You must select a video file').slideDown('fast');
        return false;
    }
    //advarsel om img/thumb ikke er lastet opp
    if(!thumbFile) {
        $alert.addClass('alert-danger').text('You must select a video thumbnail').slideDown('fast');
        return false;
    }

    // advarsel om ikke både chapter og captions er valgt
    if( (subFiles[0] || subFiles[1])  && (!subFiles[0] || !subFiles[1]) ) {
        $alert.addClass('alert-danger').text('You must select both chapter and caption subtitles').slideDown('fast');
        return false;
    }

    // advarsel om subtitle språk ikke er valgt
    if( (subFiles[0] && subFiles[1]) && subLang == '' ) {
        $alert.addClass('alert-danger').text('You must select a language').slideDown('fast');
        return false;
    }

    // Construct form data
    videoForm = new FormData();
    videoForm.append('video', vidFile);
    videoForm.append('video[]', title);
    videoForm.append('video[]', description);
    videoForm.append('thumbnail', thumbFile);

    // Add subtitles
    $.each(subFiles, function(i, sub) {
        videoForm.append('sub[]', sub);
        videoForm.append('sub[]', subLang[i]);
    });

    $.ajax({
        // Event listener for progress bar
        xhr: function() {
            $('.progress').fadeIn('fast');
            $progressbar = $('.progress-bar');

            var xhr = new window.XMLHttpRequest();
            // Get progress bar
            xhr.upload.addEventListener("progress", function(evt){
                // Calculate progress
                if (evt.lengthComputable) {
                    var percentComplete = Math.round(evt.loaded / evt.total * 100);
                    $progressbar.attr('aria-valuenow', percentComplete).css('width', percentComplete + '%').html(percentComplete + '%');
                }
            }, false);
            return xhr;
        },
        url: 'server/php/upload.php?function=uploadVideo',
        type: 'POST',
        data: videoForm,
        cache: false,
        processData: false,
        contentType: false,
        success: function(response)
        {
            // check if we get an uploaded videoid as response, and redirect to video
            if ($.isNumeric(response)) {
                $content.html(getVideo(response));
            } else {
                console.log(response);
                $alert.addClass('alert-danger').text(response).slideDown('fast');
            }
        },
        error: function(response)
        {
            console.log(response);
            $alert.removeClass('alert-danger').addClass('alert-success').text(response).slideDown('fast');
        }
    });
});