# IMT2291 Prosjekt 2 #

## Deltakere ##
* Håkon Legernæs
* Niclas Hellesen
* Erlend Brækken
* Henrik Torres

## Creating the database ##
Start by creating the database. A file has been created to automatically create the database for you. The file is named "database.php" and will set up the database and the tables required to run the project.
## The database account ##
The root account and root account password for the database was made by you or your IT administrator when the database was installed. If it is necessary to set a username and password for the admin account on the database, you may see in the top of the installer script two hardcoded variables. Edit the values in these to the name and password you prefer the database to start with.
## The table structure ##

## IMPORTANT ##
If you get "permission denied" error in PHP when uploading a video, you must change the owner of the "files" directory so Apache can write to it.
I.e. for an XAMPP installation do: "sudo chown -R daemon:daemon files"