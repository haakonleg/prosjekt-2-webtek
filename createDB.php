<?php

/*
 * This PHP script will set up the database and create a new admin user
 * so that the web application can be used. Please change the EMAIL, USERNAME,
 * and PASSWORD constants below to configure the parameters of the admin user.
*/

// WEBSITE
// Set desired username and password here which will be used for the admin user
define('EMAIL', 'admin@admin.com');
define('USERNAME', 'admin');
define('PASSWORD', 'admin');
define('USERCLASS', 'admin');

// DATABASE
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "wwwtek_prosjekt2";

try {
    $conn = new PDO("mysql:host=$servername;", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    // make sure the database is droped
    $sql = "DROP DATABASE IF EXISTS $dbname";
    // use exec() because no results are returned
    $conn->exec($sql);
    
    $sql = "CREATE DATABASE $dbname";
    // use exec() because no results are returned
    $conn->exec($sql);
    echo "Database created successfully<br>";
    }
catch(PDOException $e)
    {
    echo $sql . "<br>" . $e->getMessage();
    }

$conn = null;

try {
    
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    $sql = file_get_contents('database.sql');

    // Prepare statement
    $stmt = $conn->prepare($sql);
    // execute the query
    $stmt->execute();

    print_r ($stmt->errorInfo());
    
    
    $stmt = $conn->prepare('INSERT INTO user(username, email, password, userclass) VALUES (:username, :email, :password, :userclass)');
        $stmt->execute(array(
            ':username' => USERNAME,
            ':email' => EMAIL,
            ':password' => password_hash(PASSWORD, PASSWORD_DEFAULT),
			':userclass' => USERCLASS
        )); 
    
    }
catch(PDOException $e)
    {
    echo "<pre>".$sql . "<br>" . $e->getMessage();
    }

$conn = null;