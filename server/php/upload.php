<?php
require_once('connectDb.php');
require_once('user.php');

// Restrict access only to logged in users
if ( !$user->isLoggedIn() ) {
    die('unauthorized');
}

// Do requested action
switch($_GET['function']) {
    case 'getLanguages':
        echo(json_encode(getLanguages()));
        break;
    case 'uploadVideo':
        // Prevent error message if chapters/captions not uploaded
        if (!isset($_FILES['sub']) || !isset($_POST['sub'])) {
            $_FILES['sub'] = null;
            $_POST['sub'] = null;    
        }
        echo(uploadVideo($user, $_FILES['video'], $_FILES['thumbnail'], $_POST['video'][0], $_POST['video'][1], $_FILES['sub'], $_POST['sub']));
        break;
    default:
        echo('unknown function');
        break;
}

//Henter inn inn språk og tekst til video
function getLanguages() {
    try
    {
        $db = connectDb();
        $stmt = $db->prepare('SELECT lang, name FROM subtitlelang ORDER BY name');
        $stmt->execute();
    } catch (PDOException $e) {
        die('database error');
    }
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}


function uploadVideo($user, $videoFile, $thumbnailFile, $title, $description, $subFiles, $subLang)
{

    $db = connectDb();
    
    // Update video table to the database
    try
    {
        $stmt = $db->prepare('INSERT INTO video(userid, title, description, date) 
            VALUES (:userid, :title, :description, :date)');
        $stmt->execute(array(
            ':userid' => $user->userid,
            ':title' => $title,
            ':description' => $description,
            ':date' => date("Y-m-d H:i:s")
        ));
    } catch (PDOException $e) {
            return 'database error';
    }

    // Save videoid which is the last auto increment ID
    $videoid = $db->lastInsertId();

    // Update subtitle table
    if (isset($subFiles) && isset($subLang)) {
        foreach($subLang as $lang) {
            try
            {
                $stmt = $db->prepare('INSERT INTO subtitle(videoid, lang) VALUES (:videoid, :lang)');
                $stmt->execute(array(
                    ':videoid' => $videoid,
                    ':lang' => $lang
                ));
            } catch (PDOException $e) {
                return 'database error';
            }
        }
    }

    // Try to create new folder to put video files in
    if (mkdir('../../uploads/' . $videoid)) {
        $dir = '../../uploads/' . $videoid . '/';
    } else {
        return 'error creating directory';
    }

    // Try to upload files to server
    if (    move_uploaded_file($videoFile['tmp_name'], $dir . 'video.' . strtolower(pathinfo($videoFile['name'], PATHINFO_EXTENSION)))
        &&  move_uploaded_file($thumbnailFile['tmp_name'], $dir . 'thumbnail.' . strtolower(pathinfo($thumbnailFile['name'], PATHINFO_EXTENSION)))  ){
        if ( isset($subFiles) && isset($subLang) ) {
            // Upload subtitles
            if (!move_uploaded_file($subFiles['tmp_name'][0], $dir . 'caption_' . $subLang[0] . '.vtt')) {
                return 'error uploading subtitles';
            }
            if (!move_uploaded_file($subFiles['tmp_name'][1], $dir . 'chapter_' . $subLang[0] . '.vtt')) {
                return 'error uploading subtitles';
            }
        }
    }
    else
        return 'error uploading video';

    // Everything ok, return videoid
    return $videoid;
}

?>