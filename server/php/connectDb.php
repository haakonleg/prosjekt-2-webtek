<?php
/* This is default values for development (XAMPP).
   Change values here as necessary to connect to the server and the database.
   This file is kept as simple as possible.
*/

define ('HOSTNAME' , 'localhost');
define ('DATABASE' , 'wwwtek_prosjekt2');
define ('DBUSER' , 'root');
define ('DBPASSWORD' , '');

// Do not show error messages when not in development.
function connectDB()
{
    try {
        // Creating a database object using PHP Database Objects (PDO).
        return $db = new PDO('mysql:host=' . HOSTNAME. ';dbname=' . DATABASE . ';charset=utf8', DBUSER, DBPASSWORD);
    } catch (PDOException $e) {
       // if (isset($debug)) {
            die ('Unable to connect to database: ' . $e->getMessage());
       // } else
       //     die('Could not connect to database. Please try again later.');
    }
}
