<?php
/*
Denne filen har som formål i hente en bestemt .vtt fil sitt innhold.
Denne brukes av en funksjon i online editoren som heter "edit eksisterende".
"edit eksisterende" bruker også editVtt.php.
*/

/////////////////////////////////////////////
// Inkluderinger:                          //
/////////////////////////////////////////////
require_once('user.php');

// TODO: Debug:
//echo "PHP fil kjører nå.";

///////////////////////////////////////////// 
// Restrict access only to logged in users //
/////////////////////////////////////////////
if ( !$user->isLoggedIn() ) {
    die('unauthorized');
}

/////////////////////////////////////////////
// Variabler:                              //
/////////////////////////////////////////////
$filename     = $_POST['filename'];                  // Språk og filnavn er det samme.
$filetype     = $_POST['filetype'];                  // Standard er: .vtt
$videoID      = $_POST['videoID']['video'];          // Video ID.
$folderPath   = "/uploads/" . $videoID . "/";        // Mappen .vtt fil skal legges i.
$folderPath   = '../..' . $folderPath;               // Path til uploads mappen på server.
$pathAndName  = $folderPath . $filename; //. $filetype;
$fileContent;                                        // Vil inneholde innholdet i filen.


// Debug:
/*
echo " - ";
echo "Printer php variabel filetype:   $filetype";
echo " - ";
echo "Printer php variabel videoID:    $videoID";
echo " - ";
echo "Printer php variabel folderPath: $folderPath";
*/

/////////////////////////////////////////////
// Funksjoner:                             //
/////////////////////////////////////////////

// Hent ut de språkene som finnes på disk.
function getContent($pathAndName)
{
    // Lagrer innholdet i tekstfilen i variabel.
    //$fileContent =  glob($globArgument, $folderPath);
    $fileContent = file_get_contents($pathAndName);
    
    $fileContent = json_encode($fileContent);
    echo "$fileContent";
}

// Kjører funksjonen over.
getContent($pathAndName);

?>
