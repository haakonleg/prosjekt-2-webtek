<?php
/*
    USER.PHP
    This class contains the user framework
*/

require_once('connectDb.php');

class User
{
    /*
    * CONSTANTS
    */
    // Defines the user session timeout in minutes (will only timeout if the user is idle)
    const SESSIONTIMEOUT = 60;
    // Defines the persistent cookie timeout in days (also known as "remember me")
    const COOKIETIMEOUT = 7;
    
    /*
    * VARIABLES
    */
    var $db;
    var $userid;
    var $username;
    var $email;
    var $userclass;
    
    /*
    * CONSTRUCTOR
    * Initiates a connection to the database and starts a session
    */
    function __construct()
    {
        $this->db = connectDb();
        session_start();
    }
    
    /*
    * isLoggedIn()
    * Function checks whether user is logged in or not
    *
    * Return value: true if user is already logged in, false if user is not logged in
    */
    function isLoggedIn()
    {
        // If session variable is set
        if (isset($_SESSION['userid']))
        {
            // Authenticate the session
            if ($this->authSession())
            {
                return true;
            }
        }
        
        // Or if persisent cookie is set
        else if (isset($_COOKIE['identifier']))
        {
            // Authenticate the cookie
            if ($this->authCookie($_COOKIE['identifier'], $_COOKIE['token']))
            {
                if ($this->authSession())
                {
                    return true;
                }
            } else {
                die('Invalid session cookie, please delete your cookies.');
            }
        }
        
        return false;
    }
    
    /*
    * loginUser()
    * Takes three inputs: email, password and rememberme variable if the user
    * exists in the database, the password is verified, if rememberme is checked
    * the function makes a call to create the persistent cookie
    *
    * Return value: true/false on success/fail
    */
    function loginUser($email, $password, $rememberme)
    {
        // Try to fetch associative array
        try
        {
        $stmt = $this->db->prepare('SELECT * from user WHERE email=:email');
        $stmt->execute(array(':email' => $email ));
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
          die('Database error');
        }

        if (password_verify($password, $row['password']))
        {
            // Create a new session
            $this->createSession($row['userid']);
            // "Remember me" is checked, so create a persistent cookie
            if ($rememberme == 'checked')
            {
                $this->createPersistentCookie($row['userid']);
            }
            return true;
        }

        return false;
    }
    
    /*
    * createSession()
    * Creates a new user session and sets its timeout
    */
    private function createSession($userid)
    {
        $_SESSION['userid'] = $userid;
        // Set session timeout of 1 hour
        $_SESSION['timeout'] = time() + (60 * self::SESSIONTIMEOUT);
    }
    
    /*
    * createPersistentCookie()
    * Creates a new persistent cookie (also known as "remember me")
    *
    * Based on: http://jaspan.com/improved_persistent_login_cookie_best_practice
    */
    private function createPersistentCookie($id)
    {
        // Generate random series identifier of length 32*2 = 64 bytes
        $identifier = bin2hex(openssl_random_pseudo_bytes(32));
        // Generate random token of length 124*2 = 248 bytes, this should be more than enough
        $token = bin2hex(openssl_random_pseudo_bytes(124));
        
        setcookie('identifier', $identifier, time() + (86400 * self::COOKIETIMEOUT));
        setcookie('token', $token, time() + (86400 * self::COOKIETIMEOUT));
        
        $stmt = $this->db->prepare('INSERT into usersession(userid, seriesid, tokenid) VALUES (:userid, :seriesid, :tokenid)');
        try
        {
            $stmt->execute(array(
            ':userid' => $id,
            ':seriesid' => $identifier,
            // Hash token before inserting into database
            ':tokenid' => hash('sha256', $token)
            ));
        } catch(PDOException $e) {
            die('Database error');
        }
    }
    
    /*
    * authCookie()
    * Authenticates a persistent cookie
    * Verifies that the identifier string matches the one in the database
    * then it hashes the token and verifies that it matches the hashed token in the database
    * if so, the user is considered authenticated and is granted a new token and session
    * 
    * Based on: http://jaspan.com/improved_persistent_login_cookie_best_practice
    */
    private function authCookie($identifier, $token)
    {
        $stmt = $this->db->prepare('SELECT * from usersession WHERE seriesid=:identifier');
        $stmt->execute(array(':identifier' => $identifier ));
        if ($row = $stmt->fetch(PDO::FETCH_ASSOC))
        {
            // Verify that token hash matches the token hash in the database
            if (hash('sha256', $token) == $row['tokenid'])
            {
                // Generate new random token
                $newtoken = bin2hex(openssl_random_pseudo_bytes(124));
                // Update database with new token
                try
                {
                    $stmt = $this->db->prepare('UPDATE usersession SET tokenid=:newtoken WHERE seriesid=:identifier');
                    $stmt->execute(array(
                    ':newtoken' => hash('sha256', $newtoken),
                    ':identifier' => $identifier
                    ));
                } catch(PDOException $e) {
                    die('Database error');
                }
                // Update cookie with new token, and create new session
                setcookie('identifier', $identifier, time() + (86400 * self::COOKIETIMEOUT));
                setcookie('token', $newtoken, time() + (86400 * self::COOKIETIMEOUT));
                $this->createSession($row['userid']);
                return true;
            }
        }
        return false;
    }
    
    /*
    * authSession()
    * Authenticates the user session
    * Verifies that the session has not timed out and the user exists in the database, then resets the session timeout
    * Also fetches the username and email address
    *
    * Return value: true if session is valid, false if expired
    */
    private function authSession()
    {
        // Verify that session has not timed out
        if ($_SESSION['timeout'] > time())
        {
            $stmt = $this->db->prepare('SELECT userid, userclass, username, email from user WHERE userid=:userid');
            $stmt->execute(array(':userid' => $_SESSION['userid']));
            if ($row = $stmt->fetch(PDO::FETCH_ASSOC))
            {
                $this->userid = $row['userid'];
                $this->userclass = $row['userclass'];
                $this->username = $row['username'];
                $this->email = $row['email'];
                $_SESSION['timeout'] = time() + (60 * self::SESSIONTIMEOUT);
                return true;
            }
        }
        $GLOBALS['errormsg'] = 'Session expired, please log in again.';
        session_destroy();
        return false;
    }
    
    /*
    * logOut()
    * Signs the user out by destroying the session, and deleting the persistent cookie if it is set
    */
    function logOut()
    {
        if(isset($_COOKIE['identifier']))
        {
            try {
                // Delete cookie from database
                $stmt = $this->db->prepare('DELETE FROM usersession WHERE seriesid=:identifier');
                $stmt->execute(array(':identifier' => $_COOKIE['identifier']));
                // Delete session cookie
                setcookie('identifier', '', time() - 3600);
                setcookie('token', '', time() - 3600);
            } catch(PDOException $e) {
                    die('Database error');
            }
        }
        if (session_destroy()) return true;
        else return false;
    }
    
    /*
    * getUserList()
    * Fetch an associative array of the details of all users except the current signed in user
    * For security reasons this function should only be called when user is already signed in as an admin
    *
    * Return value: array if succsessful, false if unsucsessfull
    */
    function getUserList()
    {
        try
        {
            $stmt = $this->db->prepare('SELECT userid, userclass, username, email FROM user WHERE userid!=:currentuserid ORDER BY userid');
            $stmt->execute(array(':currentuserid' => $this->userid));
        } catch (PDOException $e) {
             die('Database error');
        }
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    /* 
    * addUser()
    * Takes three inputs: email, username and password
    * and inserts a new user into the database
    *
    * Return value: true/false on success/fail
    */
    function addUser($email, $username, $password, $userclass)
    {
        $stmt = $this->db->prepare('INSERT INTO user(username, email, password, userclass) VALUES (:username, :email, :password, :userclass)');
        if($stmt->execute(array(
            ':username' => $username,
			':email' => $email,
			':password' => password_hash($password, PASSWORD_DEFAULT),
            ':userclass' => $userclass
		))) return true;
        return false;
    }
    
    /*
    * deleteUser()
    * Takes a unique userid as input and tries to delete the user from the database
    * For security reasons this function should only be called when the user is already signed in as admin
    *
    * Return value: true/false on success/fail
    */
    function deleteUser($userid)
    {
        try
        {
            $stmt = $this->db->prepare('DELETE FROM user WHERE userid=:userid');
            $stmt->execute(array(':userid' => $userid ));
        } catch (PDOException $e) {
            return false;
        }
        return true;
    }
}

/*
 * Create new user object
 */
$user = new User();
?>