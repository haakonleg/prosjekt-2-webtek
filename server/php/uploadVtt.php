<?php
// Denne filen mottar ajax dataene som skal bli til .vtt filen.
require_once('connectDb.php');
require_once('user.php');

// TODO: Debug:
//echo "PHP fil kjører nå.";

// Restrict access only to logged in users
if ( !$user->isLoggedIn() ) {
    die('unauthorized');
}
// Variabel definisjon
$filename    = $_POST['filename'];                  // Språk og filnavn er det samme.
$filetype    = $_POST['filetype'];                  // Standard er: .vtt
$content     = $_POST['contents'];                  // Innholdet i underteksten.
$videoID     = $_POST['videoID']['video'];          // Video ID.
$folderPath  = "/uploads/" . $videoID . "/";        // Mappen .vtt fil skal legges i.
$folderPath  = '../..' . $folderPath;               // Path til uploads mappen på server.
$file        = $folderPath . $filename . $filetype; // $file inneholder path + filnavn + filtype.


// TODO: Debug:
/*
echo "\n\n";
echo "Printer php variabel filename:   $filename";
echo "\n\n__________________________   \n\n";
echo "Printer php variabel filetype:   $filetype";
echo "\n\n__________________________   \n\n";
echo "Printer php variabel contents:   $content";
echo "\n\n__________________________   \n\n";
echo "Printer php variabel videoID:    $videoID";
echo "\n\n__________________________   \n\n";
echo "Printer php variabel folderPath: $folderPath";
echo "\n\n__________________________   \n\n";
*/


// Henter ut språk kode:
switch ($filename)
{
    case "norBokmaal": $langShort = "NO";   break;
    case "norNynorsk": $langShort = "NO";   break;
    case "engelskUK":  $langShort = "EN";   break;
    case "engelskUS":  $langShort = "EN";   break;
    case "tysk":       $langShort = "DE";   break;
    case "japanese":   $langShort = "JP";   break;
}

// Debug:
echo "Fra PHP inneholder nå langShort: $langShort";


// Oppdaterer databasen.
function setLanguage($videoID, $langShort)
{
    try
    {
        $db = connectDb();
        $stmt = $db->prepare("INSERT INTO subtitle (videoid, lang) VALUES(:videoid, :lang)");
        $stmt->execute(array(
            ':videoid' => $videoID,
            ':lang' => $langShort
        ));
	print_r ($db->errorInfo());
    } catch (PDOException $e) // TODO: Ta bort etter development.
    {
        die('database error');
    }
    echo "Skal nå være lagret. Språk lagret var $langShort. VideoID: $videoID";
}

// Kjører "oppdater database funksjonen".
setLanguage($videoID, $langShort);

// Lagrer filen på server.
file_put_contents($file, $content);

?>
