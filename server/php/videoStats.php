<?php
require_once('connectDb.php');
require_once('user.php');

if ( !$user->isLoggedIn() ) {
    die('unauthorized');
}


// Do requested action
switch($_GET['function']) {
    case 'updateStats':
		//updateStats($_POST['userID'],$_POST['videoID']);
		updateStats($user->userid,$_POST['videoID']);
		break;
    case 'getStats':
        getStats($_POST['emmneKode'],$_POST['videoID']);
        break;
	case 'getEK':
		getEK();
		break;
	case 'getVid':
		getVid();
		break;
    default:
        echo('unknown function');
        break;
}

function getVid () 
	{
		$db = connectDb();
        try
        {
			
            $stmt = $db->prepare('SELECT videoid, title FROM `video`');
            $stmt->execute();
            $vid = $stmt->fetchAll(PDO::FETCH_ASSOC);
			echo json_encode($vid);
			
        } catch (PDOException $e) {
            $errormsg = 'Error: could not contact database';
        }
	}

function getEK () 
	{
		$db = connectDb();
        try
        {
			
            $stmt = $db->prepare('SELECT emnekode FROM `emne`');
            $stmt->execute();
            $EK = $stmt->fetchAll(PDO::FETCH_ASSOC);
			echo json_encode($EK);
			
        } catch (PDOException $e) {
            $errormsg = 'Error: could not contact database';
        }
	}

function updateStats($userid, $videoid)
    {
		$db = connectDb();
        try
        {
			//INSERT INTO `videostats` (`userid`, `videoid`) VALUES ('5', '1');
            $stmt = $db->prepare('INSERT INTO `videostats` (`userid`, `videoid`) VALUES (:userid, :videoid);');
            $stmt->execute(array(
				':userid'  => $userid,
				':videoid' => $videoid
			));
            
        } catch (PDOException $e) {
            $errormsg = 'Error: could not contact database';
        }
    }
	
	//Gets who is in a course that has watched video
	//videoid is either an int or an array of ints
function getStats($emmnekode, $videoid)
	{
		if (is_int($videoid) or is_string($videoid)){
			$videoid = array($videoid);
		}
		$db = connectDb();
        try
        {
			//Select user.username, video.title
			//From emne
			//inner join pameldt on emne.emnekode=pameldt.emnekode
			//inner join videostats on pameldt.userid=videostats.userid
			//inner join video on videostats.videoid=video.videoid
			//inner join user on pameldt.userid=user.userid
			//WHERE 1=1
			//and videostats.videoid in (1,2)
			//and emne.emnekode = 'IMT3021'
			
            $stmt = $db->prepare("
				Select user.username, video.title
				From emne
				inner join pameldt on emne.emnekode=pameldt.emnekode
				inner join videostats on pameldt.userid=videostats.userid
				inner join video on videostats.videoid=video.videoid
				inner join user on pameldt.userid=user.userid
				WHERE 1=1
				and videostats.videoid in (". implode(", ", $videoid) .")
				and emne.emnekode = :emmnekode 
			");
            $stmt->execute(array(
				':emmnekode'  => $emmnekode,
				
			));
			$watched = $stmt->fetchAll(PDO::FETCH_ASSOC);
			echo json_encode($watched);
			//var_dump($watched);
            
        } catch (PDOException $e) {
            $errormsg = 'Error: could not contact database';
        }	
	}
?>