<?php
require_once('user.php');

// If user is trying to log in
if ( isset($_POST['email']) && isset($_POST['password']) )
{
    if(isset($_POST['rememberme']))
    {
        $rememberme = 'checked';
    } else {
        $rememberme = '';
    }

    $user->loginUser($_POST['email'], $_POST['password'], $rememberme);
}

// Check if user is łogged in
if ($user->isLoggedIn())
{
    // Return user info
    $response = array(
        'username' => $user->username,
        'userclass' => $user->userclass
    );
    echo(json_encode($response));
}
?>