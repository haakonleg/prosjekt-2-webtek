<?php
/*
    ADMIN.PHP
    This php script controls all functionality that is used by the admin user
*/

require_once('user.php');

// Restrict access only to admin users
if ( !$user->isLoggedIn() || $user->userclass != 'admin' ) {
    die('unauthorized');
}

// Do requested action
switch($_POST['function']) {
    case 'getUserList':
        echo(json_encode($user->getUserList()));
        break;
    case 'newUser':
        if ($user->addUser($_POST['email'], $_POST['username'], $_POST['password'], $_POST['userclass']))
            echo('ok');
        else
            echo('error');
        break;
    case 'delUser':
        if($user->deleteUser($_POST['userid']))
            echo('ok');
        else
            echo('error');
        break;
    default:
        echo('function not found');
        break;
}
?>