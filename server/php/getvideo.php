<?php
/*
    GETVIDEO.PHP
    Sends an SQL statement to get details about a video
*/

require_once('user.php');
require_once('connectDb.php');

// Restrict access only to logged in users
if (!$user->isLoggedIn()) {
    die('unauthorized');
}

$db = connectDb();
$video = array();

try
{
    $stmt = $db->prepare('SELECT * FROM video WHERE videoid = :videoid');
    $stmt->execute(array(':videoid' => $_POST['videoid']));
    $video = $stmt->fetchAll(PDO::FETCH_ASSOC);
} catch (PDOException $e) {
    die('database error');
}

if ($video) {
    // Add video filename
    $video[0]['videoFile'] = str_replace('../../', '', (glob('../../uploads/' . $video[0]['videoid'] . '/video.*')[0]));
    
    // Add caption file
    if ((glob('../../uploads/' . $video[0]['videoid'] . '/caption_*.vtt')))
        $video[0]['captionFile'] = str_replace('../../', '', (glob('../../uploads/' . $video[0]['videoid'] . '/caption_*.vtt')[0]));

    // Add chapter file
    if ((glob('../../uploads/' . $video[0]['videoid'] . '/chapter_*.vtt')))
        $video[0]['chapterFile'] = str_replace('../../', '', (glob('../../uploads/' . $video[0]['videoid'] . '/chapter_*.vtt')[0]));
    
    // Add thumbnail file
    $video[0]['thumbFile'] = str_replace('../../', '', (glob('../../uploads/' . $video[0]['videoid'] . '/thumbnail.*')[0]));
    echo(json_encode($video));
} else {
    echo 'error';
}
?>