<?php
/*
    SEARCH.PHP
    Simple script to search for videos in the database using a query and returns an array of videoids
*/

require_once('connectDb.php');

$query = $_POST['searchquery'];
$result = array();

try
{
    $db = connectDb();
    $stmt = $db->prepare('SELECT videoid FROM video WHERE LCASE(title) LIKE :query OR LCASE(description) LIKE :query');
    $stmt->execute(array(':query' => '%' . $query . '%'));
    $result = json_encode(array_values($stmt->fetchAll(PDO::FETCH_NUM)));
} catch (PDOException $e) {
    die('database error');
}
echo $result;
?>