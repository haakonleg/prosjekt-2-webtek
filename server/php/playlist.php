<?php
require_once('connectDb.php');
require_once('user.php');

// Restrict access only to logged in users
if ( !$user->isLoggedIn() ) {
    die('unauthorized');
}

// Do requested action
switch($_GET['function']) {
    case 'createPlaylist':
        echo(createPlaylist($_POST['name'], $_POST['description'], $user->userid));
        break;
    case 'getPlaylists':
        echo(getPlaylists($user->userid));
        break;
    case 'getPlaylistVideos':
        echo(getPlaylistVideos($_POST['plID']));
        break;
    case 'delPlaylist':
        echo(delPlaylist($_POST['plID']));
        break;
    case 'addVideo':
        echo(addVideo($_POST['plID'], $_POST['videoID']));
        break;
    default:
        echo('unknown function');
        break;
}

// Creates a playlist
function createPlaylist($name, $description, $userid) {
    try {
        $db = connectDb();
        $stmt = $db->prepare('INSERT INTO playlist(name, description, userid) VALUES (?, ?, ?)');
        $stmt->execute(array($name, $description, $userid));
    } catch(PDOException $e) {
        die('database error');
    }
    echo('ok');
}

// Retrieves all playlists belonging to specified user
function getPlaylists($userid) {
    try {
        $db = connectDb();
        $stmt = $db->prepare('SELECT playlistid, name, description FROM playlist WHERE userid = ?');
        $stmt->execute(array($userid));
        return json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
    } catch(PDOException $e) {
        die('database error');
    }
}

// Retrives an array of videoIDs belonging to specified playlist
function getPlaylistVideos($plID) {
    try {
        $db = connectDb();
        $stmt = $db->prepare('SELECT videoid FROM playlistvideo WHERE playlistid = ?');
        $stmt->execute(array($plID));
        return json_encode($stmt->fetchAll(PDO::FETCH_NUM));
    } catch(PDOException $e) {
        die('database error');
    }
}

function delPlaylist($plID) {
    // remove playlist from db
    try {
        $db = connectDb();
        $stmt = $db->prepare('DELETE FROM playlistvideo WHERE playlistid = ?');
        $stmt->execute(array($plID));
    } catch(PDOException $e) {
        die('database error');
    }

    try {
        $db = connectDb();
        $stmt = $db->prepare('DELETE FROM playlist WHERE playlistid = ?');
        $stmt->execute(array($plID));
    } catch(PDOException $e) {
        die('database error');
    }
    echo('ok');
}

function addVideo($plID, $videoID) {
    try {
        $db = connectDb();
        $stmt = $db->prepare('INSERT INTO playlistvideo(playlistid, videoid) VALUES (?, ?)');
        $stmt->execute(array($plID, $videoID));
    } catch(PDOException $e) {
        die('database error');
    }
    echo('ok');
}
?>