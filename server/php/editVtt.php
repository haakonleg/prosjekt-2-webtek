<?php
// Denne filen gjør det mulig for bruker å editere en video.

/////////////////////////////////////////////
// Inkluderinger:                          //
/////////////////////////////////////////////
require_once('user.php');

// TODO: Debug:
//echo "PHP fil kjører nå.";

///////////////////////////////////////////// 
// Restrict access only to logged in users //
/////////////////////////////////////////////
if ( !$user->isLoggedIn() ) {
    die('unauthorized');
}

/////////////////////////////////////////////
// Variabler:                              //
/////////////////////////////////////////////
$filename;//  = $_POST['filename'];                  // Språk og filnavn er det samme.
$filetype     = $_POST['filetype'];                  // Standard er: .vtt
//$content    = $_POST['contents'];                  // Innholdet i underteksten.
$videoID      = $_POST['videoID']['video'];          // Video ID.
$folderPath   = "/uploads/" . $videoID . "/";        // Mappen .vtt fil skal legges i.
$folderPath   = '../..' . $folderPath;               // Path til uploads mappen på server.
//$file       = $folderPath . $filename . $filetype; // $file inneholder path + filnavn + filtype.
$globArgument = $folderPath . "/*" . $filetype;      // Argument til glob.


// Debug:
/*
echo " - ";
echo "Printer php variabel filetype:   $filetype";
echo " - ";
echo "Printer php variabel videoID:    $videoID";
echo " - ";
echo "Printer php variabel folderPath: $folderPath";
*/

/////////////////////////////////////////////
// Funksjoner:                             //
/////////////////////////////////////////////

// Hent ut de språkene som finnes på disk.
function getLanguages($globArgument, $folderPath)
{
// Scanner directory for filer av en gitt type (.vtt)
// foreach kodesnutten er hentet fra: http://stackoverflow.com/questions/8541180/best-way-to-get-files-from-a-dir-filtered-by-certain-extension-in    -php
    $files = array();
    foreach (glob($globArgument) as $file) {
        $file = substr($file, strlen($folderPath)+1); // Selvskreven linje. Kutter bort det som ikke er filnavn.
        // Uncomment denne dersom det ikke er ønskelig å liste kapittler.
        /*
        if (!strpos($file, 'chapter'))
        {
            $files[] = $file;
        }
        */
        $files[] = $file;
    }
    $files = json_encode($files);
    echo "$files";
}
// Kjører funksjonen over.
getLanguages($globArgument, $folderPath);

?>
