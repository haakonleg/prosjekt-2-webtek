<?php
require_once('connectDb.php');
require_once('user.php');

// Restrict access only to logged in users
if ( !$user->isLoggedIn() ) {
    die('unauthorized');
}

// Do requested action
switch($_GET['function']) {
    case 'getVideos':
        echo(json_encode(getVideos($user->userid)));
        break;
    case 'delVideo':
        echo(delVideo($_POST['videoid'], $user->userid));
        break;
    default:
        echo('unknown function');
        break;
}

// Sletter en mappe rekursivt (som rm -R)
// Kode hentet fra svar på stackoverflow: http://stackoverflow.com/a/3338133
 function rrmdir($dir) { 
   if (is_dir($dir)) { 
     $objects = scandir($dir); 
     foreach ($objects as $object) { 
       if ($object != "." && $object != "..") { 
         if (is_dir($dir."/".$object))
           rrmdir($dir."/".$object);
         else
           unlink($dir."/".$object); 
       } 
     }
     rmdir($dir); 
   } 
 }

// Gets a list of all videoids of videos uploaded by this user
function getVideos($userid) {
    $db = connectDb();
    
    try
    {
        $stmt = $db->prepare('SELECT videoid FROM video WHERE userid=? ORDER BY date DESC LIMIT 15');
        $stmt->execute(array($userid));
        return json_encode(array_values($stmt->fetchAll(PDO::FETCH_NUM)));
    } catch (PDOException $e) {
        die('database error');
    }
}

// Deletes a video of videoid belonging to this user
function delVideo($videoid, $userid) {
    $db = connectDb();

    // Check if the user really is the owner of this video
    try
    {
        $stmt = $db->prepare('SELECT userid FROM video WHERE videoid=?');
        $stmt->execute(array($videoid));
        if ($stmt->fetchAll(PDO::FETCH_NUM)[0][0] != $userid) {
            die('unauthorized');
        }
    } catch (PDOException $e) {
        die('database error');
    }

    // Delete from stats table
    try {
        $stmt = $db->prepare('DELETE FROM videostats WHERE videoid=?');
        $stmt->execute(array($videoid));
    } catch (PDOException $e) {
        die('database error');
    }

    // Delete from subtitle table
    try
    {
        $stmt = $db->prepare('DELETE FROM subtitle WHERE videoid=?');
        $stmt->execute(array($videoid));
    } catch (PDOException $e) {
        die('database error');
    }

    // Delete from video table
    try
    {
        $stmt = $db->prepare('DELETE FROM video WHERE videoid=?');
        $stmt->execute(array($videoid));
    } catch (PDOException $e) {
        die('database error');
    }

    // Delete folder and files from server
    rrmdir('../../uploads/' . $videoid . '');
    echo('ok');
}
?>